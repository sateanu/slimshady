
// Shader-ul de fragment / Fragment shader  
 
 #version 400

in vec4 ex_Color;
in vec3 FragPos;  
in vec3 Normal; 
in vec2 UV;
out vec4 out_Color;

uniform sampler2D textureMap;
uniform sampler2D normMap;

void main(void)
{
	
	out_Color = ex_Color*texture(textureMap,vec2(UV.x,1-UV.y));
}
 