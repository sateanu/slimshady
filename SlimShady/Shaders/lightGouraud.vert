// Shader-ul de varfuri  
 
 #version 400


layout(location=0) in vec4 in_Position;
layout(location=1) in vec4 in_Color;
layout(location=2) in vec3 in_Normal;
layout(location=3) in vec2 in_UV;

out vec4 gl_Position; 
out vec3 Normal;
out vec3 FragPos;
out vec4 ex_Color;
out vec2 UV;
 
uniform vec3 lightColor;
uniform vec3 lightPos; 
uniform vec3 viewPos;
uniform vec3 ambientalColor;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;


void main(void)
  {
  FragPos = vec3(model * in_Position);
  gl_Position = projection*view*model*in_Position;
    Normal=mat3(transpose(inverse(model))) *in_Normal; 
  // Ambient
	float ambientStrength = 1.0f;
	vec3 ambient = ambientStrength * ambientalColor;
  	
	// Diffuse 
	vec3 norm = normalize(Normal);
	vec3 lightDir = normalize(lightPos - FragPos);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * lightColor;
    
	// Specular
	float specularStrength =0.5f;
	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 reflectDir = reflect(-lightDir, norm);  
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
	vec3 specular = specularStrength * spec * lightColor;  
        
	vec3 result = (ambient + diffuse + specular);
    
	
	ex_Color=vec4(result,1);
	UV=in_UV;
   } 
 