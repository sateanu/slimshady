
// Shader-ul de fragment / Fragment shader  
 
 #version 400

in vec4 ex_Color;
in vec2 gl_PointCoord;
in vec3 ex_Normal;
uniform vec3 lightPos;
out vec4 out_Color;

 

void main(void)
  {
  float intensity;
	vec4 color;
	intensity = dot(lightPos,normalize(ex_Normal));

	if (intensity > 0.95)
		color = vec4(1.0,0.5,0.5,1.0);
	else if (intensity > 0.5)
		color = vec4(0.6,0.3,0.3,1.0);
	else if (intensity > 0.25)
		color = vec4(0.4,0.2,0.2,1.0);
	else
		color = vec4(0.2,0.1,0.1,1.0);
  		out_Color =  color;
  }
 