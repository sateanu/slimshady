﻿#version 330 compatibility
layout(triangles) in;
layout(line_strip, max_vertices=6) out;
 
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float time;
in VertexAttrib
{
 vec3 normal;
 vec4 color;
} vertex[];

out vec4 ex_Color;

void main()
{	
	const vec4 green = vec4(0.0f, 1.0f, 0.0f, 1.0f);
    const vec4 blue = vec4(0.0f, 0.0f, 1.0f, 1.0f);

  for(int i=0; i<3; i++)
  {
    vec3 P=gl_in[i].gl_Position.xyz;
    vec3 N=vertex[i].normal;
    gl_Position = projection*view*model*vec4(P,1);
    ex_Color=green;
    EmitVertex();
	
    gl_Position=projection*view*model*vec4(P+N*0.2,1);
    ex_Color=blue;
    EmitVertex();

    EndPrimitive();
  }
  
  
}