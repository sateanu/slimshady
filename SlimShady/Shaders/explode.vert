﻿#version 400

layout(location=0) in vec4 in_Position;
layout(location=1) in vec4 in_Color;
layout(location=2) in vec3 in_Normal;
 
out vec4 gl_Position; 
out vec4 ex_Color;

out VertexAttrib
{
 vec3 normal;
 vec4 color;
} vertex;
 

 


void main(void)
  {
    gl_Position = in_Position;
    vertex.normal=in_Normal;
    vertex.color=in_Color;

   } 
 