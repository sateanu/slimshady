﻿#version 330 compatibility
layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;
 
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float time;
uniform float normal_length=.21;
in VertexAttrib
{
 vec3 normal;
 vec4 color;
} vertex[];

out vec4 ex_Color;

void main()
{	
 
    vec3 P0=gl_in[0].gl_Position.xyz;
    vec3 P1=gl_in[1].gl_Position.xyz;
    vec3 P2=gl_in[2].gl_Position.xyz;
    vec3 V0=P0-P1;
    vec3 V1=P2-P1;
    vec3 diff=V1-V0;
   // vec3 N=normalize(vertex[0].normal.xyz);
    float diff_len = length(diff);
  
  vec3 N = normalize(cross(V1, V0));
 
  //------ Generate a new face along the direction of the face normal
  // only if diff_len is not too small.
  //
  if (length(diff_len) > 0.001)
  {
    for(int i=0; i<gl_in.length(); i++)
    {
      vec4 P = gl_in[i].gl_Position;
      vec3 N = normalize(cross(V1, V0));
      float len = sqrt(P.x*P.x + P.z*P.z);
      float scale = 2.0 + 1 * cos(time*0.5 + len);
      P = vec4(P.xyz + (N * normal_length * scale) + (N * vec3(0.05, 0.05, 0.05)), 1.0);
      gl_Position = projection*view*model* P;
      ex_Color = vertex[i].color;
      EmitVertex();
    }
    EndPrimitive();
  }
} 