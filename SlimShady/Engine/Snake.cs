﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using SlimShady.GameEngine;

namespace SlimShady.GameEngine
{
    class Snake : RevolutionSurfaceObject
    {
        public Snake(Engine engine)
            : base(engine,
                  (x, y) => (float)((1 - y) * (3 + Math.Cos(x)) * Math.Cos(2 * Math.PI * y)),
                  (x, y) => (float)(6 * y + (1 - y) * Math.Sin(x)),
                  (x, y) => (float)((1 - y) * (3 + Math.Cos(x)) * Math.Sin(2 * Math.PI * y)),
                  new Vector2(0, (float)Math.PI * 2), new Vector2(0, 1), 30, 30, false, false)
        {

        }
        public Snake(Engine engine, int curvePoints=30, int rotPoints=30, bool smooth = false, bool indexed = false) 
            : base(engine,
                  (x, y) => (float)((1 - y) * (3 + Math.Cos(x)) * Math.Cos(2 * Math.PI * y)),
                  (x, y) => (float)(6*y+(1-y)*Math.Sin(x)),
                  (x, y) => (float)((1 - y) * (3 + Math.Cos(x)) * Math.Sin(2 * Math.PI * y)),
                  new Vector2(0,(float)Math.PI*2), new Vector2(0,1), curvePoints, rotPoints, smooth, indexed)
        {

        }
    }
}
