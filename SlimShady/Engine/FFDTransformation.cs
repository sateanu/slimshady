﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SlimShady.GameEngine
{
    class FFDTransformation
    {
        Vector3[] controlPoints;
        Vector3[] controlPoints1;
        Vector3[] controlPoints2;
        Engine engine;
        float[] coefficients;
        int noPoints;
        int noPointsOffset;
        Actor actor;
        Actor[] controlPointsActors;
        Vector3[] HyperSpaceVertices;
        public void Init(int noPoints, Actor actor, Engine engine)
        {
            noPoints--;
            this.noPoints = noPoints;
            this.actor = actor;
            this.engine = engine;


            noPointsOffset = (noPoints + 1) * (noPoints + 1) * (noPoints + 1);

            controlPointsActors = new Actor[noPointsOffset];

            HyperSpaceVertices = new Vector3[actor.Vertices.Length];
            for(int i=0;i<actor.Vertices.Length;i++)
            {
                HyperSpaceVertices[i] = ConvertVertex(actor.Vertices[i]);
            }

            for (int i = 0; i < noPointsOffset; i++)
            {
                controlPointsActors[i] = new Sphere(engine)
                {
                    Color = System.Drawing.Color.White,
                    Scale = Vector3.One * 0.05f
                };
                controlPointsActors[i].Initialize();
            }

            coefficients = new float[actor.Vertices.Length * noPointsOffset];
            InitControlPoints(ref controlPoints);
            InitControlPoints(ref controlPoints1);
            InitControlPoints(ref controlPoints2);
            CreateBernsteinCoeff(coefficients, HyperSpaceVertices);

            //ComputeNewRandomControlPointsPositions(controlPoints);
        }

        Vector3 ConvertVertex(Vector3 vertex)
        {
            Vector3 newVertex = Vector3.Zero;
            newVertex.X = (vertex.X - actor.AABMin.X) / (actor.AABMax.X - actor.AABMin.X);
            newVertex.Y = (vertex.Y - actor.AABMin.Y) / (actor.AABMax.Y - actor.AABMin.Y);
            newVertex.Z = (vertex.Z - actor.AABMin.Z) / (actor.AABMax.Z - actor.AABMin.Z);
            return newVertex;
        }

        Vector3 RevertVertex(Vector3 vertex)
        {
            Vector3 newVertex = Vector3.Zero;
            newVertex.X = vertex.X * (actor.AABMax.X - actor.AABMin.X) + actor.AABMin.X;
            newVertex.Y = vertex.Y * (actor.AABMax.Y - actor.AABMin.Y) + actor.AABMin.Y;
            newVertex.Z = vertex.Z * (actor.AABMax.Z - actor.AABMin.Z) + actor.AABMin.Z;
            return newVertex;
        }

        void UpdateControlActor()
        {
            for (int i = 0; i < noPointsOffset; i++)
            {
                controlPointsActors[i].Position = controlPoints[i];
            }
        }

        public void Update(double elapsedTime, int frame)
        {
            AnimateControlPointPositions(controlPoints1, controlPoints2, controlPoints, frame);
            ComputeVerticesPosition(controlPoints, coefficients, actor.Vertices);
            UpdateControlActor();
        }

        public void InitControlPoints(ref Vector3[] controlPoints)
        {
            int i, j, k, offset;
            offset = 0;
            controlPoints = new Vector3[noPointsOffset];
            for (k = 0; k <= noPoints; k++)
            {
                for (j = 0; j <= noPoints; j++)
                {
                    for (i = 0; i <= noPoints; i++)
                    {
                        controlPoints[offset].X = (float)i / (float)noPoints;
                        controlPoints[offset].Y = (float)j / (float)noPoints;
                        controlPoints[offset].Z = (float)k / (float)noPoints;
                        
                        offset++;
                    }
                }
            }
        }

        public void CreateBernsteinCoeff(float[] coefficients, Vector3[] vertices)
        {
            float B_j_B_k, B_k;
            float[] binom = { 1.0f, 3.0f, 3.0f, 1.0f };
            
            int offset = 0;
            for (int vertex = 0; vertex < vertices.Length; vertex++)
            {
                for (int k = 0; k <= 3; k++)
                {
                    B_k = binom[k] * (float)Math.Pow(vertices[vertex].Z, k) * (float)Math.Pow(1.0f - vertices[vertex].Z, 3 - k);
                    for (int j = 0; j <= 3; j++)
                    {
                        B_j_B_k = binom[j] * (float)Math.Pow(vertices[vertex].Y, j) * (float)Math.Pow(1.0f - vertices[vertex].Y, 3 - j) * B_k;
                        for (int i = 0; i <= 3; i++)
                        {
                            coefficients[offset] = binom[i] * (float)Math.Pow(vertices[vertex].X, i) * (float)Math.Pow(1.0f - vertices[vertex].X, 3 - i) * B_j_B_k;
                            offset++;
                        }
                    }
                }
            }
        }

        void ComputeVerticesPosition(Vector3[] control_points, float[] coefficients, Vector3[] vertices)
        {
            for (int vertex = 0; vertex < vertices.Length; vertex++)
            {
                vertices[vertex].X = 0;
                vertices[vertex].Y = 0;
                vertices[vertex].Z = 0;

                int offset = 0;
                for (int k = 0; k <= noPoints; k++)
                {
                    for (int j = 0; j <= noPoints; j++)
                    {
                        for (int i = 0; i <= noPoints; i++)
                        {

                            vertices[vertex].X += coefficients[vertex * noPointsOffset + offset] * control_points[offset].X;
                            vertices[vertex].Y += coefficients[vertex * noPointsOffset + offset] * control_points[offset].Y;
                            vertices[vertex].Z += coefficients[vertex * noPointsOffset + offset] * control_points[offset].Z;
                            offset++;
                        }
                    }
                }

                //vertices[vertex] = RevertVertex(vertices[vertex]);
            }
        }

        void ComputeNewRandomControlPointsPositions(Vector3[] control_points)
        {
            int i, j, k, offset;
            offset = 0;
            Random r = new Random((int)DateTime.Now.Ticks);
            for (k = 0; k <= noPoints; k++)
            {
                for (j = 0; j <= noPoints; j++)
                {
                    for (i = 0; i <= noPoints; i++)
                    {
                        control_points[offset].X = (float)i / noPoints + (((float)r.NextDouble()-0.5f) / 1);
                        control_points[offset].Y = (float)j / noPoints + (((float)r.NextDouble()-0.5f) / 1);
                        control_points[offset].Z = (float)k / noPoints + (((float)r.NextDouble()-0.5f) / 1);
                        offset++;
                    }
                }
            }
        }

        void AnimateControlPointPositions(Vector3[] control_points1, Vector3[] control_points2, Vector3[] control_points, int frame)
        {
            if (frame == 0)
            {
                for (int offset = 0; offset < noPointsOffset; offset++)
                {
                    control_points1[offset] = control_points2[offset];
                }

                ComputeNewRandomControlPointsPositions(control_points2);
            }

            for (int offset = 0; offset < noPointsOffset; offset++)
            {
                control_points[offset].X = ((float)frame * control_points2[offset].X + (20.0f - (float)frame) * control_points1[offset].X) / 20.0f;
                control_points[offset].Y = ((float)frame * control_points2[offset].Y + (20.0f - (float)frame) * control_points1[offset].Y) / 20.0f;
                control_points[offset].Z = ((float)frame * control_points2[offset].Z + (20.0f - (float)frame) * control_points1[offset].Z) / 20.0f;

            }

        }

        public void DrawControlPoints()
        {
            for(int i=0;i<noPointsOffset;i++)
            {
                controlPointsActors[i].Draw();
            }
        }

    }
}
