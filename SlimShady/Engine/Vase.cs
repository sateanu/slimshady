﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlimShady.GameEngine
{
    class Vase : RevolutionSurfaceObject
    {
        public Vase(Engine world) : base(world,
            (x, y) => (float)(Math.Cos(y)*(Math.Sin(x)+2)),
            (x, y) => x,
            (x, y) => (float)(Math.Sin(y)*(Math.Sin(x)+2)),
            new Vector2(0,(float)Math.PI*2), new Vector2(0, (float)Math.PI * 2),
            30,30,false,false)
        {
         
        }
        public Vase(Engine world, int curvePoints, int rotPoints, bool smooth, bool indexed) :
            base(world,
            (x, y) => (float)(Math.Cos(y) * (Math.Sin(x) + 2)),
            (x, y) => x,
            (x, y) => (float)(Math.Sin(y) * (Math.Sin(x) + 2)),
            new Vector2(0, (float)Math.PI * 2), new Vector2(0, (float)Math.PI * 2),
            curvePoints, rotPoints, smooth, indexed)
        {
        
        }
    }
}
