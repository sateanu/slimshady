﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlimShady.GameEngine
{
    class Horn : RevolutionSurfaceObject
    {
        public Horn(Engine world) : base(world,
            (x, y) => (float)(Math.Sin(y) * (1 / x)),
            (x, y) => (float)Math.Cos(x),
            (x, y) => (float)(Math.Cos(y) * (1 / x)),
            new Vector2(0, 5), new Vector2(0, (float)Math.PI * 2),
            30,30,false,false)
        {

        }
        public Horn(Engine world, int curvePoints, int rotPoints, bool smooth, bool indexed) :
            base(world,
            (x, y) => (float)(Math.Sin(y) * (1/x)),
            (x, y) => (float)Math.Cos(x),
            (x, y) => (float)(Math.Cos(y) * (1/x)),
            new Vector2(0.1f, 30), new Vector2(0, (float)Math.PI * 2),
            curvePoints, rotPoints, smooth, indexed)
        {

        }
    }
}
