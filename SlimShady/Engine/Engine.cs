﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
namespace SlimShady.GameEngine
{
    public class Engine
    {
        public Camera Camera{ get; set; }
        public Dictionary<string, Shader> Shaders;
        public string ActiveShaderName { get; set; }
        public Shader ActiveShader
        {
            get
            {
                if (Shaders.Keys.Contains(ActiveShaderName))
                {
                    return Shaders[ActiveShaderName];
                }
                else
                    return null;
            }
        }

        public float Width { get; set; }
        public float Height { get; set; }

        public Engine()
        {
            Camera = new Camera(CameraType.FirstPerson);
            Shaders = new Dictionary<string, Shader>();
        }

        internal void SelectShader(string shaderName)
        {
            ActiveShaderName = shaderName;
            if(ActiveShader!=null)
            {
                GL.LinkProgram(ActiveShader.ProgramID);
                GL.UseProgram(ActiveShader.ProgramID);
            }
        }
    }
}