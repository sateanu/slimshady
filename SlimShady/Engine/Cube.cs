﻿using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace SlimShady.GameEngine
{
    class Cube : Actor
    {
        private static readonly Vector3[] CubeVertices =
        {
            //F1
            new Vector3(1.0f, -1.0f, 1.0f),
            new Vector3(-1.0f, -1.0f, -1.0f),
            new Vector3(1.0f, -1.0f, -1.0f),

            new Vector3(-1.0f, 1.0f, -1.0f),
            new Vector3(1.0f, 1.0f, 1.0f),
            new Vector3(1.0f, 1.0f, -1.0f),
            //F2
            new Vector3(1.0f, 1.0f, -1.0f),
            new Vector3(1.0f, -1.0f, 1.0f),
            new Vector3(1.0f, -1.0f, -1.0f),

            new Vector3(1.0f, 1.0f, 1.0f),
            new Vector3(-1.0f, -1.0f, 1.0f),
            new Vector3(1.0f, -1.0f, 1.0f),
            //F3
            new Vector3(-1.0f, -1.0f, 1.0f),
            new Vector3(-1.0f, 1.0f, -1.0f),
            new Vector3(-1.0f, -1.0f, -1.0f),

            new Vector3(1.0f, -1.0f, -1.0f),
            new Vector3(-1.0f, 1.0f, -1.0f),
            new Vector3(1.0f, 1.0f, -1.0f),
            //F4
            new Vector3(1.0f, -1.0f, 1.0f),
            new Vector3(-1.0f, -1.0f, 1.0f),
            new Vector3(-1.0f, -1.0f, -1.0f),

            new Vector3(-1.0f, 1.0f, -1.0f),
            new Vector3(-1.0f, 1.0f, 1.0f),
            new Vector3(1.0f, 1.0f, 1.0f),
            //F5
            new Vector3(1.0f, 1.0f, -1.0f),
            new Vector3(1.0f, 1.0f, 1.0f),
            new Vector3(1.0f, -1.0f, 1.0f),
            
            new Vector3(1.0f, 1.0f, 1.0f),
            new Vector3(-1.0f, 1.0f, 1.0f),
            new Vector3(-1.0f, -1.0f, 1.0f),
            //F6
            new Vector3(-1.0f, -1.0f, 1.0f),
            new Vector3(-1.0f, 1.0f, 1.0f),
            new Vector3(-1.0f, 1.0f, -1.0f),

            new Vector3(1.0f, -1.0f, -1.0f),
            new Vector3(-1.0f, -1.0f, -1.0f),
            new Vector3(-1.0f, 1.0f, -1.0f),

        };
       
        private static readonly Vector3[] CubeColors =
        {
            new Vector3(1,0,0), 
            new Vector3(1,0,0), 
            new Vector3(1,0,0),

            new Vector3(0,0,1),
            new Vector3(0,0,1),
            new Vector3(0,0,1),

            new Vector3(0,1,1),
            new Vector3(0,1,1),
            new Vector3(0,1,1),

            new Vector3(1,1,0),
            new Vector3(1,1,0),
            new Vector3(1,1,0),

            new Vector3(1,0,1),
            new Vector3(1,0,1),
            new Vector3(1,0,1),

            new Vector3(0,1,0),
            new Vector3(0,1,0),
            new Vector3(0,1,0),

            new Vector3(1,0,0),
            new Vector3(1,0,0),
            new Vector3(1,0,0),

            new Vector3(0,0,1),
            new Vector3(0,0,1),
            new Vector3(0,0,1),

            new Vector3(0,1,1),
            new Vector3(0,1,1),
            new Vector3(0,1,1),

            new Vector3(1,1,0),
            new Vector3(1,1,0),
            new Vector3(1,1,0),

            new Vector3(1,0,1),
            new Vector3(1,0,1),
            new Vector3(1,0,1),

            new Vector3(0,1,0),
            new Vector3(0,1,0),
            new Vector3(0,1,0),

        };

        private static Vector3[] CubeNormals =
        {
            new Vector3(0,-1,0), 
            new Vector3(0,-1,0), 
            new Vector3(0,-1,0), 

            new Vector3(0,1,0), 
            new Vector3(0,1,0), 
            new Vector3(0,1,0), 

            new Vector3(1,0,0), 
            new Vector3(1,0,0), 
            new Vector3(1,0,0), 

            new Vector3(0,0,1), 
            new Vector3(0,0,1), 
            new Vector3(0,0,1), 

            new Vector3(-1,0,0), 
            new Vector3(-1,0,0), 
            new Vector3(-1,0,0), 

            new Vector3(0,0,-1), 
            new Vector3(0,0,-1), 
            new Vector3(0,0,-1), 

            new Vector3(0,-1,0), 
            new Vector3(0,-1,0), 
            new Vector3(0,-1,0), 

            new Vector3(0,1,0), 
            new Vector3(0,1,0), 
            new Vector3(0,1,0), 

            new Vector3(1,0,0), 
            new Vector3(1,0,0), 
            new Vector3(1,0,0), 

            new Vector3(0,0,1), 
            new Vector3(0,0,1), 
            new Vector3(0,0,1), 

            new Vector3(-1,0,0), 
            new Vector3(-1,0,0), 
            new Vector3(-1,0,0), 

            new Vector3(0,0,-1), 
            new Vector3(0,0,-1), 
            new Vector3(0,0,-1), 
        };

        private static int[] CubeIndexes =
        {
            0,1,2,3,4,5,
            6,7,8,9,10,11,
            12,13,14,15,16,17,
            18,19,20,21,22,23,
            24,25,26,27,28,29,
            30,31,32,33,34,35
        };

        private static Vector2[] CubeUV =
        {
           new Vector2(1,0), 
           new Vector2(0,1), 
           new Vector2(0,0), 

           new Vector2(1,0), 
           new Vector2(0,1), 
           new Vector2(0,0), 

           new Vector2(1,0),
           new Vector2(0,1), 
           new Vector2(0,0), 

           new Vector2(1,0), 
           new Vector2(0,1), 
           new Vector2(0,0), 

           new Vector2(0,0), 
           new Vector2(1,1), 
           new Vector2(0,1),
            
           new Vector2(1,0), 
           new Vector2(0,1), 
           new Vector2(0,0), 

           new Vector2(1,0), 
           new Vector2(1,1), 
           new Vector2(0,1), 

           new Vector2(1,0), 
           new Vector2(1,1), 
           new Vector2(0,1), 

           new Vector2(1,0), 
           new Vector2(1,1), 
           new Vector2(0,1), 

           new Vector2(1,0), 
           new Vector2(1,1), 
           new Vector2(0,1),
            
           new Vector2(0,0), 
           new Vector2(1,0), 
           new Vector2(1,1), 

           new Vector2(1,0), 
           new Vector2(1,1), 
           new Vector2(0,1), 
        };
        public Cube(Engine world) : base(world)
        {
            this.Vertices =(Vector3[])CubeVertices.Clone();
            this.Colors = CubeColors;
            this.Normals = (Vector3[])CubeNormals.Clone();
            this.Indexes = CubeIndexes;
            this.UV = CubeUV;
        }

        public override void Draw()
        {
            base.Draw(PrimitiveType.Triangles);
        }
    }
}
