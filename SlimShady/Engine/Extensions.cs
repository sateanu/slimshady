﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlimShady.GameEngine
{
    public static class Extensions
    {
        public static float[] ToFloatArray(this List<Vector3> list)
        {
            float[] array = new float[list.Count * 3];

            for (int i = 0; i < list.Count; i++)
            {
                array[i*3] = list[i].X;
                array[i*3+1] = list[i].Y;
                array[i*3+2] = list[i].Z;
            }

            return array;
        }

        public static bool HasNaNInfinity(this Vector3 vec3)
        {
            bool valid = (float.IsNaN(vec3.X) || float.IsInfinity(vec3.X) ||
                float.IsNaN(vec3.Y) || float.IsInfinity(vec3.Y) ||
                float.IsNaN(vec3.Z) || float.IsInfinity(vec3.Z));

            return valid;
        }
    }
}
