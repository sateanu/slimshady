﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace SlimShady.GameEngine
{
    class Sphere : RevolutionSurfaceObject
    {
        public Sphere(Engine world) : base(world,
            (x, y) => (float)(Math.Sin(y)*(Math.Sin(x))),
            (x, y) => (float)Math.Cos(x),
            (x, y) => (float)(Math.Cos(y)*(Math.Sin(x))),
            new Vector2(0,(float)Math.PI), new Vector2(0, (float)Math.PI * 2),
            30,30,false,false)
        {

        }
        public Sphere(Engine world, int curvePoints, int rotPoints, bool smooth, bool indexed) :
            base(world,
            (x, y) => (float)(Math.Sin(y) * (Math.Sin(x))),
            (x, y) => (float)Math.Cos(x),
            (x, y) => (float)(Math.Cos(y) * (Math.Sin(x))),
            new Vector2(0, (float)Math.PI), new Vector2(0, (float)Math.PI * 2),
            curvePoints, rotPoints, smooth, indexed)
        {

        }
    }
}
