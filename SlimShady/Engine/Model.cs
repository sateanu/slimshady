﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using SlimShady.GameEngine;

namespace SlimShady.GameEngine
{
    public class Model : Actor
    {
        public Model(Engine engine) : base(engine)
        {

        }
        List<Vector3> verticesData = new List<Vector3>();
        List<Vector3> normalsData = new List<Vector3>();
        List<Vector2> UVData = new List<Vector2>();
        List<int> verticesIndexes = new List<int>();
        List<int> normalIndexes = new List<int>();
        List<int> UVIndexes = new List<int>();

        public Model(Engine engine, string path) : base(engine)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                
                string line = reader.ReadLine();
                //Read the model
                while (line != null)
                {
                    if (line == "") { line = reader.ReadLine(); continue; }
                    char[] separators = new char[] { ' ' };
                    string[] parts = line.Trim().Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    float x = 0, y = 0, z = 0;
                    switch (parts[0])
                    {
                        case "v":
                            //VERTEX COORDS
                            x = float.Parse(parts[1], CultureInfo.InvariantCulture);
                            y = float.Parse(parts[2], CultureInfo.InvariantCulture);
                            z = float.Parse(parts[3], CultureInfo.InvariantCulture);
                            verticesData.Add(new Vector3(x, y, z));
                            break;
                        case "vt":
                            //TEXTURE COORDS
                            x = float.Parse(parts[1], CultureInfo.InvariantCulture);
                            y = float.Parse(parts[2], CultureInfo.InvariantCulture);
                            UVData.Add(new Vector2(x,y));
                            break;
                        case "vn":
                            x = float.Parse(parts[1], CultureInfo.InvariantCulture);
                            y = float.Parse(parts[2], CultureInfo.InvariantCulture);
                            z = float.Parse(parts[3], CultureInfo.InvariantCulture);
                            normalsData.Add(new Vector3(x, y, z));
                            //NORMAL COORDS
                            break;
                        case "f":
                            switch (parts.Length)
                            {
                                case 5:
                                    LoadFace(parts[1]);
                                    LoadFace(parts[2]);
                                    LoadFace(parts[3]);

                                    LoadFace(parts[1]);
                                    LoadFace(parts[3]);
                                    LoadFace(parts[4]);
                                    break;
                                case 4:
                                    LoadFace(parts[1]);
                                    LoadFace(parts[2]);
                                    LoadFace(parts[3]);
                                    break;
                            }
                                
                            break;
                    }
                    line = reader.ReadLine();
                }
                //Start processing the model
                List<Vector3> vertices=new List<Vector3>();
                List<Vector3> normals=new List<Vector3>();
                List<Vector3> colors=new List<Vector3>();
                List<Vector2> UVs=new List<Vector2>();
                List<int> indexes=new List<int>();
                for (int i = 0; i < verticesIndexes.Count; i++)
                {
                    vertices.Add(verticesData[verticesIndexes[i]-1]);
                    normals.Add(normalsData[normalIndexes[i]-1]);
                    colors.Add(verticesData[verticesIndexes[i] - 1]);
                    if(UVData.Count>0)
                        UVs.Add(UVData[UVIndexes[i]-1]);
                    indexes.Add(verticesIndexes[i]-1);
                }

                Vertices = vertices.ToArray();
                Normals = normals.ToArray();
                UV = UVs.ToArray();
                //Colors = colors.ToArray();
                //Indexes = indexes.ToArray();
            }
        }

        private void LoadFace(string face)
        {
            string[] indexesData = face.Split('/');
            switch (indexesData.Length)
            {
                case 1:
                    verticesIndexes.Add(int.Parse(indexesData[0]));
                    break;
                case 2:
                    verticesIndexes.Add(int.Parse(indexesData[0]));
                    UVIndexes.Add(int.Parse(indexesData[1]));
                    break;
                case 3:
                    verticesIndexes.Add(int.Parse(indexesData[0]));
                    if (indexesData[1] != "")
                        UVIndexes.Add(int.Parse(indexesData[1]));
                    normalIndexes.Add(int.Parse(indexesData[2]));
                    break;
            }
        }

        public override void Draw()
        {
            Draw(PrimitiveType.Triangles);
        }

        public static Model LoadModel(Engine engine,string path)
        {
            Model mod=new Model(engine);
            using (StreamReader reader=new StreamReader(path))
            {
                List<Vector3> vertices=new List<Vector3>();
                List<Vector3> normals=new List<Vector3>();
                List<int> verticesIndexes=new List<int>();
                List<int> normalIndexes = new List<int>();
                string line = reader.ReadLine();
                while (line!=null)
                {
                    string[] parts = line.Split(' ');
                    float x=0, y=0, z=0;
                    switch (parts[0])
                    {
                        case "v":
                            //VERTEX COORDS
                            x = float.Parse(parts[1], CultureInfo.InvariantCulture);
                            y = float.Parse(parts[2], CultureInfo.InvariantCulture);
                            z = float.Parse(parts[3], CultureInfo.InvariantCulture);
                            vertices.Add(new Vector3(x,y,z));
                            break;
                        case "vt":
                            //TEXTURE COORDS
                            break;
                        case "vn":
                            x = float.Parse(parts[1], CultureInfo.InvariantCulture);
                            y = float.Parse(parts[2], CultureInfo.InvariantCulture);
                            z = float.Parse(parts[3], CultureInfo.InvariantCulture);
                            vertices.Add(new Vector3(x, y, z));
                            //NORMAL COORDS
                            break;
                        case "f":
                            for (int i = 1; i < parts.Length; i++)
                            {
                                string face = parts[i];
                                string[] indexes = face.Split('/');
                                switch (indexes.Length)
                                {
                                    case 1:
                                        verticesIndexes.Add(int.Parse(indexes[0]));
                                        break;
                                    case 2:
                                        verticesIndexes.Add(int.Parse(indexes[0]));
                                        break;
                                    case 3:
                                        verticesIndexes.Add(int.Parse(indexes[0]));
                                        normalIndexes.Add(int.Parse(indexes[2]));
                                        break;

                                }
                            }
                            break;

                    }


                    line = reader.ReadLine();
                }
                
            }
            return mod;
        }
    }
}
