﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace SlimShady.GameEngine
{
    public class RevolutionSurfaceObject : Actor
    {
        public RevolutionSurfaceObject(Engine engine,
            Func<float, float, float> funcX,
            Func<float, float, float> funcY,
            Func<float, float, float> funcZ, Vector2 domX, Vector2 domY,
            int curvePoints, int rotPoints, bool smooth = false, bool indexed = false) : base(engine)
        {
            RevolutionSurfaceParametric surfGen = new RevolutionSurfaceParametric(curvePoints, rotPoints,
                Vector3.One, domX, domY, funcX, funcY, funcZ);
            surfGen.Generate(smooth);

            Vertices = surfGen.vertices.ToArray();
            Colors = surfGen.colors.ToArray();
            Normals = surfGen.normals.ToArray();
            UV = surfGen.UVs.ToArray();
            if (indexed)
                Indexes = surfGen.indexes.ToArray();
        }

        public RevolutionSurfaceObject(Engine engine,
            string funcX, string funcY,string funcZ, Vector2 domX, Vector2 domY,
            int curvePoints, int rotPoints, bool smooth = false, bool indexed = false) : base(engine)
        {
            RevolutionSurfaceExpression surfGen = new RevolutionSurfaceExpression(curvePoints, rotPoints,
                Vector3.One, domX, domY, funcX, funcY, funcZ);

            surfGen.Generate(smooth);

            Vertices = surfGen.vertices.ToArray();
            Colors = surfGen.colors.ToArray();
            Normals = surfGen.normals.ToArray();
            UV = surfGen.UVs.ToArray();
            if (indexed)
                Indexes = surfGen.indexes.ToArray();
        }

        public override void Draw()
        {
            base.Draw(PrimitiveType.Triangles);
        }
    }
}
