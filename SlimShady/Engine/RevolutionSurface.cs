﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlimShady.GameEngine
{
    class RevolutionSurface
    {
        float curvePointsCount;
        float rotPointsCount;
        Vector3 color;
        Func<float, float> function;
        Vector3 rotAxis;
        Vector2 dom;
        public List<Vector3> vertices;
        public List<Vector3> colors;
        public List<Vector3> normals;
        public List<int> indexes;
        private Dictionary<Vector3, int> vertexIndex;
        //For each vertex the faces containing it
        private Dictionary<Vector3, List<Face>> VertexAdjFaces;

        public RevolutionSurface(float curvePointsCount,float rotPointsCount,Vector3 color,Vector3 rotAxis, Vector2 dom, Func<float, float> function)
        {
            this.curvePointsCount = curvePointsCount;
            this.rotPointsCount = rotPointsCount;
            this.color = color;
            this.function = function;
            this.rotAxis = rotAxis.Normalized();
            vertices = new List<Vector3>();
            colors = new List<Vector3>();
            normals = new List<Vector3>();
            indexes=new List<int>();
            vertexIndex=new Dictionary<Vector3, int>();
            this.dom = dom;
            VertexAdjFaces=new Dictionary<Vector3, List<Face>>();
        }

        public void Generate(bool smooth=true)
        {
            float step = (float)Math.PI * 2 / rotPointsCount;
            if (dom.X > dom.Y)
            {
                float aux = dom.X;
                dom.X = dom.Y;
                dom.Y = aux;
            }

            float curveStep = (dom.Y - dom.X) / (curvePointsCount);
            int index = 0;
            for (int j = 0; j <= curvePointsCount; j++)
            {
                for (int i = 0; i <= rotPointsCount; i++)
                {
                    float xPos = j * curveStep + dom.X;
                    float xNextPos = xPos + curveStep;

                    float yPos = i * step;
                    float yNextPos = ((i + 1)) * step;

                    float res = function(xPos);
                    float resNext = function(xNextPos);

                    var point1 = new Vector3((float) Math.Cos(yPos)*res, xPos, (float) Math.Sin(yPos)*res);
                    var point2 = new Vector3((float) Math.Cos(yPos)*resNext, xNextPos, (float) Math.Sin(yPos)*resNext);
                    var point3 = new Vector3((float) Math.Cos(yNextPos)*res, xPos, (float) Math.Sin(yNextPos)*res);
                    var point4 = new Vector3((float) Math.Cos(yNextPos)*resNext, xNextPos,
                        (float) Math.Sin(yNextPos)*resNext);

                    AddVertex(point1,ref index);
                    AddVertex(point2,ref index);
                    AddVertex(point3,ref index);

                    AddVertex(point3,ref index);
                    AddVertex(point2,ref index);
                    AddVertex(point4,ref index);

                    Face f1 = new Face() { A = point1, B = point2, C = point3 };
                    Face f2 = new Face() { A = point1, B = point2, C = point3 };

                    if (!VertexAdjFaces.ContainsKey(point1))
                        VertexAdjFaces.Add(point1, new List<Face>());
                    if (!VertexAdjFaces.ContainsKey(point2))
                        VertexAdjFaces.Add(point2, new List<Face>());
                    if (!VertexAdjFaces.ContainsKey(point3))
                        VertexAdjFaces.Add(point3, new List<Face>());
                    if (!VertexAdjFaces.ContainsKey(point4))
                        VertexAdjFaces.Add(point4, new List<Face>());

                    //First face
                    VertexAdjFaces[point1].Add(f1);
                    VertexAdjFaces[point2].Add(f1);
                    VertexAdjFaces[point3].Add(f1);
                    //Second face
                    VertexAdjFaces[point3].Add(f2);
                    VertexAdjFaces[point2].Add(f2);
                    VertexAdjFaces[point4].Add(f2);
                }
            }

            //if (!smooth)
            {
                //Normals - Flat
                for (int i = 0; i + 3 <= indexes.Count(); i = i + 3)
                {
                    Vector3 n1 = vertices[indexes[i]];
                    Vector3 n2 = vertices[indexes[i + 1]];
                    Vector3 n3 = vertices[indexes[i + 2]];
                    Vector3 n;
                    n = Vector3.Normalize(Vector3.Cross(n2 - n1, n3 - n1));
                    normals.Add(n);
                    normals.Add(n);
                    normals.Add(n);
                }
            }
            if(smooth)
            {
                //Normals - Smooth
                for (int i = 0; i < indexes.Count(); i++)
                {
                    Vector3 n = new Vector3();
                    Vector3 vert = vertices[indexes[i]];
                    //for (int j = 0; j + 3 <= indexes.Count; j = j + 3)
                    //{
                    //    Vector3 v1 = vertices[indexes[j]];
                    //    Vector3 v2 = vertices[indexes[j + 1]];
                    //    Vector3 v3 = vertices[indexes[j + 2]];
                    //    if (vert == v1 || vert == v2 || vert == v3)
                    //    {
                    //        n += Vector3.Cross(v2 - v1, v3 - v1);
                    //    }
                    //}
                    foreach (var face in VertexAdjFaces[vert])
                    {
                        n += face.GetNormal();
                    }
                    n.Normalize();
                    normals[indexes[i]]+=n;
                }
            }


            for (int i = 0; i < indexes.Count; i++)
            {
                colors.Add(new Vector3((float)(vertices[indexes[i]].X),
                       (float)(vertices[indexes[i]].Y),
                       (float)(vertices[indexes[i]].Z)));
                //colors.Add(color);
            }
            
        }

        private void AddVertex(Vector3 vertex, ref int index)
        {
            //TO-DO: Fix this vertex stuff
            //if (vertexIndex.ContainsKey(vertex))
            //{
            //    indexes.Add(vertexIndex[vertex]);
            //}
            //else
            {
                vertices.Add(vertex);
                //vertexIndex.Add(vertex, index);
                indexes.Add(index++);
            }
        }

    }
}
