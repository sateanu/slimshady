﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlimShady.GameEngine
{
    class Cilinder : RevolutionSurfaceObject
    {
        public Cilinder(Engine engine) : base(engine,
            (x,y)=>(float)Math.Cos(y),
            (x,y)=>x,
            (x,y)=>(float)Math.Sin(y),
            new Vector2(-1,1),new Vector2(0,(float)Math.PI),30,30,false,false) 
        {
            
        }

        public Cilinder(Engine engine, int curvePoints, int rotPoints, bool smooth = false,
            bool indexed = false)
            : base(engine,
                (x, y) => (float) Math.Cos(y),
                (x, y) => x,
                (x, y) => (float) Math.Sin(y),
                new Vector2(-1, 1), new Vector2(0, (float)Math.PI*2), curvePoints, rotPoints, smooth, indexed)
        {

        }
    }
}
