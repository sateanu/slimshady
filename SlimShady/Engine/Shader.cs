﻿using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SlimShady.GameEngine
{
    public class Shader
    {
        public int ProgramID = -1;
        public int VertexID = -1;
        public int FragmentID = -1;
        public int GeometryID = -1;
        public string VertexLog = "";
        public string FragmentLog = "";
        public string GeometryLog = "";
        public string VertexShader = "";
        public string FragmentShader = "";
        public string GeometryShader = "";
        public Shader()
        {
            ProgramID = GL.CreateProgram();
        }

        private void loadShader(string shader, ShaderType type, out int address, out string log)
        {
            address = GL.CreateShader(type);
            GL.ShaderSource(address, shader);
            GL.CompileShader(address);
            GL.AttachShader(ProgramID, address);
            GL.GetShaderInfoLog(address,out log);
        }

        private void LoadShaderFromString(string shader, ShaderType type)
        {
            if (type == ShaderType.VertexShader)
            {
                VertexShader = shader;
                loadShader(shader, type, out VertexID, out VertexLog);
            }
            else if (type == ShaderType.FragmentShader)
            {
                FragmentShader = shader;
                loadShader(shader, type, out FragmentID, out FragmentLog);
            }
            else if (type == ShaderType.GeometryShader)
            {
                GeometryShader = shader;
                loadShader(shader,type,out GeometryID,out GeometryLog);
            }
        }

        private void LoadShaderFromFile(string path, ShaderType type)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                if (type == ShaderType.VertexShader)
                {
                    VertexShader = reader.ReadToEnd();
                    loadShader(VertexShader, type, out VertexID, out VertexLog);
                }
                else if (type == ShaderType.FragmentShader)
                {
                    FragmentShader = reader.ReadToEnd();
                    loadShader(FragmentShader, type, out FragmentID, out FragmentLog);
                }
                else if (type == ShaderType.GeometryShader)
                {
                    GeometryShader = reader.ReadToEnd();
                    loadShader(GeometryShader, type, out GeometryID, out GeometryLog);
                }
            }
        }

        public Shader(string vshader, string fshader, bool file = false)
        {
            ProgramID = GL.CreateProgram();

            if (file)
            {
                LoadShaderFromFile(vshader, ShaderType.VertexShader);
                LoadShaderFromFile(fshader, ShaderType.FragmentShader);
            }
            else
            {
                LoadShaderFromString(vshader, ShaderType.VertexShader);
                LoadShaderFromString(fshader, ShaderType.FragmentShader);
            }
        }

        public Shader(string vshader, string fshader,string gshader, bool file = false)
        {
            ProgramID = GL.CreateProgram();

            if (file)
            {
                LoadShaderFromFile(vshader, ShaderType.VertexShader);
                LoadShaderFromFile(fshader, ShaderType.FragmentShader);
                LoadShaderFromFile(gshader, ShaderType.GeometryShader);
            }
            else
            {
                LoadShaderFromString(vshader, ShaderType.VertexShader);
                LoadShaderFromString(fshader, ShaderType.FragmentShader);
                LoadShaderFromString(gshader, ShaderType.GeometryShader);
            }
        }
    }
}
