﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
namespace SlimShady.GameEngine
{
    public enum CameraType
    {
        FirstPerson,
        Orbit
    }
    public class Camera
    {
        public Vector3 Position;

        public Vector3 Rotation;

        public Matrix4 Projection { get; set; }

        public Matrix4 RotationMatrix
        {
            get
            {
                Matrix4 camRot = Matrix4.CreateRotationX(Rotation.X) * Matrix4.CreateRotationY(Rotation.Y) * Matrix4.CreateRotationZ(Rotation.Z);
                return camRot;
            }
        }

        public Matrix4 View
        {
            get
            {
                if (camType == CameraType.FirstPerson)
                {
                    return FirstPersonView();

                }
                else if(camType==CameraType.Orbit)
                {
                    return OrbitalView();
                }
                return FirstPersonView();

            }
        }
        public float OrbitalRadius { get; set; }
        private Matrix4 OrbitalView()
        {
            Vector3 eye=Vector3.Zero;
            eye.X = Position.X + OrbitalRadius*(float)(Math.Cos(Rotation.X) * Math.Sin(Rotation.Y));
            eye.Z = Position.Z + OrbitalRadius*(float)(Math.Sin(Rotation.X) * Math.Sin(Rotation.Y));
            eye.Y = Position.Y + OrbitalRadius*(float)(Math.Cos(Rotation.Y));
            Vector3 cameraUpVector = new Vector3(0, 1, 0);
            Matrix4 view = Matrix4.LookAt(eye, Position, cameraUpVector);

            return view;
        }

        private Matrix4 FirstPersonView()
        {
            Matrix4 camRot = RotationMatrix;
            Vector3 cameraOriginalTarget = new Vector3(0, 0, -1.0f);
            Vector3 cameraRotatedTarget = Vector3.Transform(cameraOriginalTarget, camRot);
            Vector3 cameraFinalTarget = Position + cameraRotatedTarget;

            Vector3 cameraUpVector = new Vector3(0, 1, 0);
            Vector3 cameraRotatedUpVector = Vector3.Transform(cameraUpVector, camRot);

            Matrix4 view = Matrix4.LookAt(Position, cameraFinalTarget, cameraRotatedUpVector);

            return view;
        }

        private CameraType camType;
        public Camera(CameraType cameraType=CameraType.FirstPerson)
        {
            camType = cameraType;
            Position = Vector3.Zero;
            Rotation = Vector3.Zero;
            OrbitalRadius = 100;
        }

        internal void AddPosition(Vector3 moveVector)
        {
            Vector3 rotatedVector = Vector3.Transform(moveVector, RotationMatrix);
            Position += rotatedVector;
        }
    }
}