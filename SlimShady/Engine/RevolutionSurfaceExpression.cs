﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using SlimShady.GameEngine;
using NCalc;

namespace SlimShady.GameEngine
{
    class RevolutionSurfaceExpression
    {
        float curvePointsCount;
        float rotPointsCount;
        Vector3 color;
        string functionX;
        string functionY;
        string functionZ;
        Vector3 rotAxis;
        Vector2 domX;
        Vector2 domY;
        
        private Dictionary<Vector3, int> vertexIndex;
        public List<Vector3> vertices;
        public List<Vector3> colors;
        public List<Vector3> normals;
        public List<int> indexes;
        public List<Vector2> UVs;
        //For each vertex the faces containing it
        private Dictionary<Vector3, List<Face>> VertexAdjFaces;

        public RevolutionSurfaceExpression(float curvePointsCount, float rotPointsCount, Vector3 color, Vector2 domX, Vector2 domY,
            string functionX, string functionY, string functionZ)
        {
            this.curvePointsCount = curvePointsCount;
            this.rotPointsCount = rotPointsCount;
            this.color = color;
            this.functionX = functionX;
            this.functionY = functionY;
            this.functionZ = functionZ;
            this.rotAxis = rotAxis.Normalized();
            vertices = new List<Vector3>();
            colors = new List<Vector3>();
            normals = new List<Vector3>();
            indexes = new List<int>();
            UVs = new List<Vector2>();
            vertexIndex = new Dictionary<Vector3, int>();
            VertexAdjFaces = new Dictionary<Vector3, List<Face>>();
            this.domX = domX;
            this.domY = domY;
        }
        //t=x ; s=y;
        private float RezolveExpression(Expression e, float t, float s)
        {
            float rez = 0;

            e.Parameters["s"] = s;
            e.Parameters["t"] = t;
            e.Parameters["Pi"] = Math.PI;

            rez=(float)Convert.ToDouble(e.Evaluate());

            return rez;
        }
        public void Generate(bool smooth = false)
        {
            Expression eX=new Expression(functionX);
            Expression eY=new Expression(functionY);
            Expression eZ=new Expression(functionZ);
            if (domX.X > domX.Y)
            {
                float aux = domX.X;
                domX.X = domX.Y;
                domX.Y = aux;
            }
            if (domY.X > domY.Y)
            {
                float aux = domY.X;
                domY.X = domY.Y;
                domY.Y = aux;
            }
            int index = 0;
            float step = (domX.Y - domX.X) / rotPointsCount;
            //STEP = THETA (O CU LINIE)
            float curveStep = (domY.Y - domY.X) / curvePointsCount;
            float Vstep = 1 / rotPointsCount;
            float Ustep = 1 / curvePointsCount;
            //curveSTEP = PHI
            for (int j = 0; j < curvePointsCount; j++)
            {
                for (int i = 0; i < rotPointsCount; i++)
                {
                    float x = i * step + domX.X; //THETA / V
                    float y = j * curveStep + domY.X; //PHi / U
                    float xNext = (i + 1) * step + domX.X;
                    float yNext = (j + 1) * curveStep + domY.X;
                    float V = (1.0f - i * Vstep);
                    float VNext = (1.0f - (i + 1) * Vstep);
                    float U = j * Ustep;
                    float UNext = (j + 1) * Ustep;

                    var point1 = new Vector3(RezolveExpression(eX,x,y), RezolveExpression(eY,x, y), RezolveExpression(eZ,x, y));
                    var point2 = new Vector3(RezolveExpression(eX,xNext, y), RezolveExpression(eY,xNext, y), RezolveExpression(eZ,xNext, y));
                    var point3 = new Vector3(RezolveExpression(eX,x, yNext), RezolveExpression(eY,x, yNext), RezolveExpression(eZ,x, yNext));
                    var point4 = new Vector3(RezolveExpression(eX,xNext, yNext), RezolveExpression(eY,xNext, yNext), RezolveExpression(eZ,xNext, yNext));

                    if (point1.HasNaNInfinity() || point2.HasNaNInfinity() ||
                        point3.HasNaNInfinity() || point4.HasNaNInfinity())
                        continue;


                    AddVertex(point1, ref index);
                    AddVertex(point2, ref index);
                    AddVertex(point3, ref index);
                    UVs.Add(new Vector2(U, V));
                    UVs.Add(new Vector2(U, VNext));
                    UVs.Add(new Vector2(UNext, V));

                    AddVertex(point3, ref index);
                    AddVertex(point2, ref index);
                    AddVertex(point4, ref index);
                    UVs.Add(new Vector2(UNext, V));
                    UVs.Add(new Vector2(U, VNext));
                    UVs.Add(new Vector2(UNext, VNext));

                    Face f1 = new Face() { A = point1, B = point2, C = point3 };
                    Face f2 = new Face() { A = point1, B = point2, C = point3 };

                    if (!VertexAdjFaces.ContainsKey(point1))
                        VertexAdjFaces.Add(point1, new List<Face>());
                    if (!VertexAdjFaces.ContainsKey(point2))
                        VertexAdjFaces.Add(point2, new List<Face>());
                    if (!VertexAdjFaces.ContainsKey(point3))
                        VertexAdjFaces.Add(point3, new List<Face>());
                    if (!VertexAdjFaces.ContainsKey(point4))
                        VertexAdjFaces.Add(point4, new List<Face>());

                    //First face
                    VertexAdjFaces[point1].Add(f1);
                    VertexAdjFaces[point2].Add(f1);
                    VertexAdjFaces[point3].Add(f1);
                    //Second face
                    VertexAdjFaces[point3].Add(f2);
                    VertexAdjFaces[point2].Add(f2);
                    VertexAdjFaces[point4].Add(f2);
                }
            }

            if (!smooth)
            {
                //Normals - Flat
                for (int i = 0; i + 3 <= indexes.Count(); i = i + 3)
                {
                    Vector3 n1 = vertices[indexes[i]];
                    Vector3 n2 = vertices[indexes[i + 1]];
                    Vector3 n3 = vertices[indexes[i + 2]];
                    Vector3 n;
                    n = Vector3.Normalize(Vector3.Cross(n2 - n1, n3 - n1));
                    normals.Add(n);
                    normals.Add(n);
                    normals.Add(n);
                }
            }
            else
            {
                //Normals - Smooth
                for (int i = 0; i < indexes.Count(); i++)
                {
                    Vector3 n = new Vector3();
                    Vector3 vert = vertices[indexes[i]];
                    //for (int j = 0; j + 3 <= indexes.Count; j = j + 3)
                    //{
                    //    Vector3 v1 = vertices[indexes[j]];
                    //    Vector3 v2 = vertices[indexes[j + 1]];
                    //    Vector3 v3 = vertices[indexes[j + 2]];
                    //    if (vert == v1 || vert == v2 || vert == v3)
                    //    {
                    //        n+=Vector3.Cross(v2-v1,v3-v1);   
                    //    }
                    //}
                    foreach (var face in VertexAdjFaces[vert])
                    {
                        n += face.GetNormal();
                    }
                    n.Normalize();
                    normals.Add(n);
                }
            }
            for (int i = 0; i < indexes.Count; i++)
            {
                colors.Add(new Vector3((float)(vertices[indexes[i]].X),
                       (float)(vertices[indexes[i]].Y),
                       (float)(vertices[indexes[i]].Z)));
                //colors.Add(color);
            }

        }

        private void AddVertex(Vector3 vertex, ref int index)
        {
            //if (vertexIndex.ContainsKey(vertex))
            //{
            //    indexes.Add(vertexIndex[vertex]);
            //}
            //else
            {
                vertices.Add(vertex);
                //vertexIndex.Add(vertex,index);
                indexes.Add(index++);

            }
        }

    }
}
