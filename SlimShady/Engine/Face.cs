﻿using System;
using OpenTK;

namespace SlimShady.GameEngine
{
    internal class Face
    {
        public Vector3 A { get; set; }
        public Vector3 B { get; set; }
        public Vector3 C { get; set; }

        public Vector3 GetNormal()
        {
            Vector3 normal = Vector3.Cross(B - A, C - A);
            float sin_alpha = normal.Length/((B - A).Length*(C - A).Length);
            return normal;
        }
    }
}