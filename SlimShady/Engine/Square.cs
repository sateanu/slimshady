﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlimShady.GameEngine
{
    class Square : Actor
    {
        public readonly float[] SquareVerticesFloat =
        {
            -1,1,0,
             1,1,0,
            -1,-1,0,
             1,-1,0
        };

        public readonly Vector3[] SquareVertices =
        {
            new Vector3(-1,1,0), 
            new Vector3(1,1,0), 
            new Vector3(-1,-1,0), 
            new Vector3(1,-1,0), 
        };

        public readonly float[] SquareColors =
        {
             1,0,0,
             0,1,0,
             0,0,1,
             1,1,0
        };

        public Square(Engine world) : base(world)
        {
            Vertices = SquareVertices;
            //Colors = SquareColors;
        }

        public Square(Engine world, Vector3 color) : base(world)
        {
            Vertices = SquareVertices;
            //Colors = new float[]
            //{
            //    color.X,color.Y,color.Z,
            //    color.X,color.Y,color.Z,
            //    color.X,color.Y,color.Z,
            //    color.X,color.Y,color.Z,
            //};
        }

        public override void Draw()
        {
            this.Draw(OpenTK.Graphics.OpenGL4.PrimitiveType.TriangleStrip);
        }
    }
}
