﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Platform.Windows;
using OpenTK.Input;
using PixelFormat = OpenTK.Graphics.OpenGL4.PixelFormat;
using Rectangle = System.Drawing.Rectangle;

namespace SlimShady.GameEngine
{
    public class Actor : IDrawable, IUpdatable
    {
        public Color Color { get; set; }

        public Vector3 Position { get; set; }

        public Vector3 Rotation { get; set; }

        public Vector3 Scale { get; set; }

        public Vector3[] Vertices { get; set; }

        public Vector3[] Normals { get; set; }

        public Vector3[] Colors { get; set; }

        public Vector2[] UV { get; set; }

        public int[] Indexes { get; set; }

        public int TextureID { get; set; }

        public int NormalMapID { get; set; }

        public Vector3 AABMin;

        public Vector3 AABMax;


        public List<IObject> Children
        {
            get;
            set;
        }

        private int VaoId;
        private int vertexVbo;
        private int colorVbo;
        private int normalVbo;
        private int textureUV;
        private int indexVbo;

        protected readonly Engine engine;

        public Actor(Engine engine)
        {
            this.engine = engine;
            Position = Vector3.Zero;
            Rotation = Vector3.Zero;
            Scale = Vector3.One;
        }

        public Actor(Engine engine, Vector3[] vertices)
        {
            this.engine = engine;
            Position = Vector3.Zero;
            Rotation = Vector3.Zero;
            Scale = Vector3.One;
            this.Vertices = vertices;
        }

        public void Initialize()
        {
            int attribIndex = 0;
            VaoId = GL.GenVertexArray();
            GL.BindVertexArray(VaoId);
            vertexVbo = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexVbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vertices.Length * Vector3.SizeInBytes), Vertices, BufferUsageHint.StaticDraw);
            GL.EnableVertexAttribArray(attribIndex);
            GL.VertexAttribPointer(0,3, VertexAttribPointerType.Float, false, 0, 0);
            GL.BindBuffer(BufferTarget.ArrayBuffer,0);
            if (Colors != null && Colors.Length > 0)
            {
                colorVbo = GL.GenBuffer();
                GL.BindBuffer(BufferTarget.ArrayBuffer, colorVbo);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Colors.Length * Vector3.SizeInBytes), Colors, BufferUsageHint.StaticDraw);
                GL.EnableVertexAttribArray(1);
                GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 0, 0);
                GL.BindBuffer(BufferTarget.ArrayBuffer,0);
            }
            if (Normals != null && Normals.Length > 0)
            {
                normalVbo = GL.GenBuffer();
                GL.BindBuffer(BufferTarget.ArrayBuffer, normalVbo);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Normals.Length * Vector3.SizeInBytes), Normals, BufferUsageHint.StaticDraw);
                GL.EnableVertexAttribArray(2);
                GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, false, 0, 0);
                GL.BindBuffer(BufferTarget.ArrayBuffer,0);
            }
            if (UV != null && UV.Length > 0)
            {
                GL.BindTexture(TextureTarget.Texture2D, TextureID);
                textureUV = GL.GenBuffer();
                GL.BindBuffer(BufferTarget.ArrayBuffer, textureUV);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(UV.Length*Vector2.SizeInBytes),UV,BufferUsageHint.StaticDraw);
                GL.EnableVertexAttribArray(3);
                GL.VertexAttribPointer(3, 2, VertexAttribPointerType.Float, false, 0, 0);
                GL.BindBuffer(BufferTarget.ArrayBuffer,0);
            }
            if (Indexes != null && Indexes.Length > 0)
            {
                indexVbo = GL.GenBuffer();
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexVbo);
                GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(Indexes.Length * sizeof(int)), Indexes, BufferUsageHint.StaticDraw);
            }

            for(int i=0; i<Vertices.Length;i++)
            {
                if (Vertices[i].X > AABMax.X)
                    AABMax.X = Vertices[i].X;
                if (Vertices[i].Y > AABMax.Y)
                    AABMax.Y = Vertices[i].Y;
                if (Vertices[i].Z > AABMax.Z)
                    AABMax.Z = Vertices[i].Z;

                if (Vertices[i].X < AABMin.X)
                    AABMin.X = Vertices[i].X;
                if (Vertices[i].Y < AABMin.Y)
                    AABMin.Y = Vertices[i].Y;
                if (Vertices[i].Z < AABMin.Z)
                    AABMin.Z = Vertices[i].Z;
            }

        }

        public void UpdateVAO()
        {
            int attribIndex = 0;
            GL.BindVertexArray(VaoId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexVbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vertices.Length * Vector3.SizeInBytes), Vertices, BufferUsageHint.StaticDraw);
            GL.EnableVertexAttribArray(attribIndex);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            if (Colors != null && Colors.Length > 0)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, colorVbo);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Colors.Length * Vector3.SizeInBytes), Colors, BufferUsageHint.StaticDraw);
                GL.EnableVertexAttribArray(1);
                GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 0, 0);
                GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            }
            if (Normals != null && Normals.Length > 0)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, normalVbo);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Normals.Length * Vector3.SizeInBytes), Normals, BufferUsageHint.StaticDraw);
                GL.EnableVertexAttribArray(2);
                GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, false, 0, 0);
                GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            }
            if (UV != null && UV.Length > 0)
            {
                GL.BindTexture(TextureTarget.Texture2D, TextureID);
                GL.BindBuffer(BufferTarget.ArrayBuffer, textureUV);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(UV.Length * Vector2.SizeInBytes), UV, BufferUsageHint.StaticDraw);
                GL.EnableVertexAttribArray(3);
                GL.VertexAttribPointer(3, 2, VertexAttribPointerType.Float, false, 0, 0);
                GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            }
            if (Indexes != null && Indexes.Length > 0)
            {
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexVbo);
                GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(Indexes.Length * sizeof(int)), Indexes, BufferUsageHint.StaticDraw);
            }
        }

        public virtual void Draw(PrimitiveType type)
        {
            int programID = engine.ActiveShader.ProgramID;
            int modelMatLocation = GL.GetUniformLocation(programID, "model");
            int viewMatLocation = GL.GetUniformLocation(programID, "view");
            int projMatLocation = GL.GetUniformLocation(programID, "projection");
            int lightColorLocation = GL.GetUniformLocation(programID, "lightColor");
            int lightPosLocation = GL.GetUniformLocation(programID, "lightPos");
            int viewPosLocation = GL.GetUniformLocation(programID, "viewPos");
            int ambientalColorLocation = GL.GetUniformLocation(programID, "ambientalColor");
            int time = GL.GetUniformLocation(programID, "time");
            int texture = GL.GetUniformLocation(programID, "textureMap");
            int normalMap = GL.GetUniformLocation(programID, "normMap");
            int mouseLocation = GL.GetUniformLocation(programID, "mouse");
            int resLocation = GL.GetUniformLocation(programID, "resolution");
            Vector2 mousePos=new Vector2(Mouse.GetCursorState().X,Mouse.GetCursorState().Y);
            GL.Uniform2(mouseLocation, mousePos);
            GL.Uniform2(resLocation,new Vector2(engine.Width,engine.Height));

            Matrix4 proj = engine.Camera.Projection;
            Matrix4 view = engine.Camera.View;
            Matrix4 rot = Matrix4.CreateRotationX(Rotation.X)*
                          Matrix4.CreateRotationY(Rotation.Y)*
                          Matrix4.CreateRotationZ(Rotation.Z);

            Matrix4 model = Matrix4.CreateScale(Scale)*rot*Matrix4.CreateTranslation(Position);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.UniformMatrix4(projMatLocation, false, ref proj);
            GL.UniformMatrix4(viewMatLocation, false, ref view);
            GL.UniformMatrix4(modelMatLocation, false, ref model);
            GL.Uniform3(lightPosLocation, Globals.lightPos);
            GL.Uniform3(lightColorLocation, Globals.lightColor);
            GL.Uniform3(ambientalColorLocation, Globals.ambientColor);
            GL.Uniform3(viewPosLocation, engine.Camera.Position);
            var timenow = (float) DateTime.Now.TimeOfDay.TotalSeconds;
            GL.Uniform1(time,timenow);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, TextureID);
            GL.Uniform1(texture, TextureUnit.Texture0-TextureUnit.Texture0);
            if (NormalMapID > 0)
            {
                GL.ActiveTexture(TextureUnit.Texture1);
                GL.BindTexture(TextureTarget.Texture2D,NormalMapID);
                GL.Uniform1(normalMap,(int)TextureUnit.Texture1-(int)TextureUnit.Texture0);
            }

            GL.BindVertexArray(VaoId);
            if (Indexes != null && Indexes.Length > 0)
                GL.DrawElements(type, Indexes.Length, DrawElementsType.UnsignedInt, IntPtr.Zero);
            else
                GL.DrawArrays(type, 0, Vertices.Length);

        }

        public void LoadTexture(Bitmap texture)
        {
            TextureID = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, TextureID);
            BitmapData texData = texture.LockBits(new Rectangle(0, 0, texture.Width, texture.Height),
                    ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, texData.Width,
                texData.Height,0,PixelFormat.Bgra,PixelType.UnsignedByte, texData.Scan0);

            texture.UnlockBits(texData);
            GL.TexParameter(TextureTarget.Texture2D,TextureParameterName.TextureMinFilter,(int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D,TextureParameterName.TextureMagFilter,(int)TextureMagFilter.Linear);
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
        }

        public void LoadNormalMap(Bitmap texture)
        {
            NormalMapID = GL.GenTexture();
            
            GL.BindTexture(TextureTarget.Texture2D, NormalMapID);
            BitmapData texData = texture.LockBits(new Rectangle(0, 0, texture.Width, texture.Height),
                    ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, texData.Width,
                texData.Height, 0, PixelFormat.Bgra, PixelType.UnsignedByte, texData.Scan0);
            
            texture.UnlockBits(texData);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
        }

        public void SmoothNormals()
        {
            List<Vector3> normals=new List<Vector3>();
            if (Indexes != null && Indexes.Length > 0)
            {
                for (int i = 0; i < Indexes.Count(); i++)
                {
                    Vector3 n = new Vector3();
                    Vector3 vert = Vertices[Indexes[i]];
                    for (int j = 0; j + 3 <= Indexes.Length; j = j + 3)
                    {
                        Vector3 v1 = Vertices[Indexes[j]];
                        Vector3 v2 = Vertices[Indexes[j + 1]];
                        Vector3 v3 = Vertices[Indexes[j + 2]];
                        if (vert == v1 || vert == v2 || vert == v3)
                        {
                            Face f=new Face();
                            f.A = v1;
                            f.B = v2;
                            f.C = v3;
                            n += f.GetNormal();
                        }
                    }
                    n.Normalize();
                    Normals[Indexes[i]] = n;
                }
                
                
            }
            else
            {
                for (int i = 0; i < Vertices.Count(); i++)
                {
                    Vector3 n = new Vector3();
                    Vector3 vert = Vertices[i];
                    for (int j = 0; j + 3 <= Vertices.Length; j = j + 3)
                    {
                        Vector3 v1 = Vertices[j];
                        Vector3 v2 = Vertices[j + 1];
                        Vector3 v3 = Vertices[j + 2];
                        if (vert == v1 || vert == v2 || vert == v3)
                        {
                            Face f = new Face();
                            f.A = v1;
                            f.B = v2;
                            f.C = v3;
                            n += f.GetNormal();
                        }
                    }
                    n.Normalize();
                    normals.Add(n);
                }
                Normals = normals.ToArray();
            }
            Initialize();
        }

        public virtual void Draw() { }

        
    }
}
