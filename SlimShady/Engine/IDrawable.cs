﻿using System.Drawing;
using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace SlimShady.GameEngine
{
    public interface IDrawable : IObject
    {
        Color Color { get; set; }

        Vector3[] Vertices { get; set; }

        Vector3[] Normals { get; set; }

        Vector3[] Colors { get; set; }

        Vector2[] UV { get; set; }

        int[] Indexes { get; set; }

        void Draw(PrimitiveType type);

        int TextureID { get; set; }

        int NormalMapID { get; set; }

        void LoadTexture(Bitmap textureData);
    }
}
