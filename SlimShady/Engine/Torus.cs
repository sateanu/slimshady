﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlimShady.GameEngine
{
    class Torus : RevolutionSurfaceObject
    {
        const float R = 3f;
        const float r = 0.5f;
        
        public Torus(Engine engine) : base(engine,
            (x,y)=> (float)((Math.Cos(x) * r + R) * Math.Cos(y)),
            (x,y)=> (float)Math.Sin(x),
            (x,y)=> (float)((Math.Cos(x) * r + R) * Math.Sin(y)),
            new Vector2(0,(float)Math.PI*2), new Vector2(0, (float)Math.PI * 2),
            30,30,false,false)
        {
            
        }
        public Torus(Engine engine, int curvePoints, int rotPoints, bool smooth = false,
            bool indexed = false) 
            : base(engine,
            (x, y) => (float)((Math.Cos(x) * r + R) * Math.Cos(y)),
            (x, y) => (float)Math.Sin(x),
            (x, y) => (float)((Math.Cos(x) * r + R) * Math.Sin(y)),
            new Vector2(0, (float)Math.PI * 2), new Vector2(0, (float)Math.PI * 2),
            curvePoints, rotPoints, smooth, indexed)
        {

        }


        public override void Draw()
        {
            base.Draw(OpenTK.Graphics.OpenGL4.PrimitiveType.Triangles);
        }
    }
}
