﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SlimShady
{
    /// <summary>
    /// Interaction logic for CreateObject.xaml
    /// </summary>
    public partial class CreateObject : Window, INotifyPropertyChanged
    {
        public CreateObject()
        {
            DataContext = this;
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        private int rotPoints=30;
        private int curvePoints=30;
        private bool smooth=false;
        private bool indexed = false;

        public int RotPoints
        {
            get { return rotPoints; }
            set
            {
                rotPoints = value;
                OnPropertyChanged("RotPoints");
            }
        }

        public int CurvePoints
        {
            get { return curvePoints; }
            set
            {
                curvePoints = value;
                OnPropertyChanged("CurvePoints");
            }
        }

        public bool Smooth
        {
            get { return smooth; }
            set
            {
                smooth = value;
                OnPropertyChanged("Smooth");
            }
        }

        public bool Indexed
        {
            get { return indexed; }
            set
            {
                indexed = value;
                OnPropertyChanged("Indexed");
            }
        }

        private void createButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
