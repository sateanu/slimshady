﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;
using OpenTK;
using System.Diagnostics;
using System.IO;
using Color = System.Drawing.Color;
using System.Windows.Forms;
using OpenTK.Input;
using SlimShady.GameEngine;
using MouseButton = OpenTK.Input.MouseButton;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Security.Policy;
using System.Threading;
using Brushes = System.Windows.Media.Brushes;
using DataFormats = System.Windows.DataFormats;
using DragDropEffects = System.Windows.Forms.DragDropEffects;
using DragEventArgs = System.Windows.DragEventArgs;
using Image = System.Windows.Controls.Image;
using MenuItem = System.Windows.Controls.MenuItem;
using MessageBox = System.Windows.MessageBox;
using MouseButtonEventArgs = System.Windows.Input.MouseButtonEventArgs;
using PixelFormat = System.Drawing.Imaging.PixelFormat;
using Point = System.Windows.Point;
using Rectangle = System.Drawing.Rectangle;
using Timer = System.Windows.Forms.Timer;

namespace SlimShady
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window,INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
        bool loaded = false;
        Color c = Color.Yellow;
        Actor actor;

        public float ActorRotationX
        {
            get { return MathHelper.RadiansToDegrees(actor.Rotation.X); }
            set
            {
                actor.Rotation = new Vector3(MathHelper.DegreesToRadians(value), actor.Rotation.Y, actor.Rotation.Z);
                OnPropertyChanged("ActorRotationX");
            }
        }

        public float ActorRotationY
        {
            get { return MathHelper.RadiansToDegrees(actor.Rotation.Y); }
            set
            {
                actor.Rotation = new Vector3(actor.Rotation.X, MathHelper.DegreesToRadians(value), actor.Rotation.Z);
                OnPropertyChanged("ActorRotationY");
            }
        }

        public float ActorRotationZ
        {
            get { return MathHelper.RadiansToDegrees(actor.Rotation.Z); }
            set
            {
                actor.Rotation = new Vector3(actor.Rotation.X, actor.Rotation.Y, MathHelper.DegreesToRadians(value));
                OnPropertyChanged("ActorRotationZ");
            }
        }

        public float ActorScaleX
        {
            get { return actor.Scale.X; }
            set
            {
                actor.Scale = new Vector3(value, actor.Scale.Y, actor.Scale.Z);
                OnPropertyChanged("ActorScaleX");
            }
        }

        public float ActorScaleY
        {
            get { return actor.Scale.Y; }
            set
            {
                actor.Scale = new Vector3(actor.Scale.X, value, actor.Scale.Z);
                OnPropertyChanged("ActorScaleY");
            }
        }

        public float ActorScaleZ
        {
            get { return actor.Scale.Z; }
            set
            {
                actor.Scale = new Vector3(actor.Scale.X, actor.Scale.Y, value);
                OnPropertyChanged("ActorScaleZ");
            }
        }

        private bool wireframe;
        public bool Wireframe
        {
            get { return wireframe; }
            set
            {
                wireframe = value;
                OnPropertyChanged("Wireframe");
            }
        }

        public float Far
        {
            get { return far; }
            set
            {
                far = value;
                ResizeViewport();
                OnPropertyChanged("Far");
            }
        }

        public float Near
        {
            get { return near; }
            set
            {
                near = value;
                ResizeViewport();
                OnPropertyChanged("Near");
            }
        }

        public float Fov
        {
            get { return MathHelper.RadiansToDegrees(fov); }
            set
            {
                fov = MathHelper.DegreesToRadians(value); 
                ResizeViewport();
                OnPropertyChanged("Fov");
            }
        }

        Stopwatch sw = new Stopwatch();

        float mouseAcceleration = 0.001f;
        private float x = 0;
        float scrollAcceleration = 2.0f;
        private float far=10000f;
        private float near = 0.1f;
        Engine engine;
        Sphere lightCube;
        float walkSpeed = 10;
        float lightSpeed = 0.1f;
        MouseState previousMouse;
        double elapsedTime;
        TimeSpan previous;
        private Timer timer;
        WPFGLControl glControl;
        Dictionary<string, Type> objectsDict;
        private Dictionary<string, Shader> shadersDictionary;
        private HashSet<Type> CreateObjectAcceptedTypes;
        private Bitmap currentTexture;
        private Bitmap currentNormalTexture;
        FFDTransformation ffdTransform;

        public MainWindow()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-us");
            InitializeComponent();
            DataContext = this;
            glControl = new WPFGLControl();
            glHost.Child = glControl;
            glControl.Resize += glControl_Resize;
            glControl.Load += glControl_Load;
            glControl.Paint += glControl_Paint;
            glControl.AllowDrop = true;
            glControl.DragEnter += GlControl_DragEnter;
            glControl.DragDrop += GlControl_DragDrop;
            KeyUp += MainWindow_KeyUp;
            timer=new Timer();
            shadersDictionary=new Dictionary<string, Shader>();
            objectsDict = new Dictionary<string, Type>();
            objectsDict.Add("Torus", typeof(Torus));
            objectsDict.Add("Vase", typeof(Vase));
            objectsDict.Add("Cilinder", typeof(Cilinder));
            objectsDict.Add("Sphere", typeof(Sphere));
            objectsDict.Add("Cube", typeof(Cube));
            objectsDict.Add("Gabriel's Horn", typeof(Horn));
            objectsDict.Add("Snake", typeof(Snake));

            foreach (var type in objectsDict)
            {
                MenuItem mItem = new MenuItem();
                mItem.Header = type.Key;
                mItem.Click += MItem_Click;
                GenerateMenu.Items.Add(mItem);
            }
            
            
            CreateObjectAcceptedTypes=new HashSet<Type>();
            CreateObjectAcceptedTypes.Add(typeof (Snake));
            CreateObjectAcceptedTypes.Add(typeof (Cilinder));
            CreateObjectAcceptedTypes.Add(typeof (Torus));
            CreateObjectAcceptedTypes.Add(typeof (Vase));
            CreateObjectAcceptedTypes.Add(typeof (Sphere));
            CreateObjectAcceptedTypes.Add(typeof (Horn));
        }

        private void GlControl_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if(files.Length>0)
            {
                if (files[0].EndsWith(".obj"))
                {
                    actor = new Model(engine, files[0]);
                    actor.Initialize();
                    if (currentTexture != null)
                        actor.LoadTexture(currentTexture);
                }
                else
                {
                    MessageBox.Show("File type not accepted !", "Wrong file type", MessageBoxButton.OK,
                        MessageBoxImage.Exclamation);
                }
            }
        }

        private void GlControl_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void MItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem) sender;
            GenerateObject(item.Header.ToString());
        }

        private void MainWindow_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key==System.Windows.Input.Key.F5)
            {
                ReloadShader();
            }
        }

        private void ResizeViewport()
        {
            float width = glControl.Width;
            float height = glControl.Height;
            GL.Viewport(0, 0, (int)width, (int)height);
            engine.Width = width;
            engine.Height = height;
            engine.Camera.Projection = Matrix4.CreatePerspectiveFieldOfView(fov, glControl.AspectRatio, Near, Far);
        }
        private void glControl_Resize(object sender, EventArgs e)
        {
            
            if (!loaded)
                return;

            ResizeViewport();
        }

       
        float rotatespeed = 0.0f;
        private void DrawScene()
        {
            GL.ClearColor(Color.Black);
            GL.DepthMask(true);
            //GL.CullFace(CullFaceMode.Back);
            if (wireframe)
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            else
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            x = x + rotatespeed;
            actor.Position = new Vector3(0, 0, 0);
            /*actor.Rotation = new Vector3(MathHelper.DegreesToRadians(x), MathHelper.DegreesToRadians(x),
                MathHelper.DegreesToRadians(0));*/
            actor.Draw();
            string currentShader = engine.ActiveShaderName;

            //Set the shader for custom objects to normal
            engine.SelectShader("Default");
            lightCube.Position = Globals.lightPos;
            lightCube.Draw();

            if (ffdTransform != null)
            {
                ffdTransform.DrawControlPoints();
            }

            //Reset the shader to the current one
            engine.SelectShader(currentShader);
        }
        int frame = 0;
        private void UpdateScene()
        {
            TimeSpan current = DateTime.Now.TimeOfDay;
            elapsedTime = (current - previous).TotalSeconds;
            previous = current;

            UpdateMouse();
            UpdateKeyboard();

            if(ffdTransform != null)
            {
                frame++;
                if (frame > 20)
                    frame = 0;

                ffdTransform.Update(elapsedTime, frame);
                actor.UpdateVAO();
            }
        }

        bool WaitFPS(float fps)
        {
            if (sw.ElapsedMilliseconds >= 1000/fps)
            {
                sw.Reset();
                sw.Start();
                Console.WriteLine(true);
                return true;
            }
            Console.WriteLine(false);
            return false;
            
            
        }

        private void glControl_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            if (!loaded)
                return;
            
            UpdateScene();

            DrawScene();

            glControl.SwapBuffers();
        }

        private void glControl_Load(object sender, EventArgs e)
        {
            loaded = true;
            LoadScene();
            timer.Interval = 1000/Globals.FPS;
            timer.Tick += Timer_Tick;
            timer.Start();
            sw.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            glControl.Invalidate();
        }

        float fov = (float)Math.PI / 4;

        private void LoadScene()
        {
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Texture2D);
            engine = new Engine();
            LoadShaders();
            
            //OpenTK.Input.Mouse.SetPosition(0, 0);
            //world.Camera.Projection = Matrix4.CreatePerspectiveOffCenter(0,width, height,0, 0.1f, 100000.0f);
            //engine.Camera.Projection = Matrix4.CreatePerspectiveFieldOfView(fov, glControl.AspectRatio, Near, Far);
            ResizeViewport();

            engine.Camera.Position = new Vector3(0, 0, 15);
            engine.Camera.Rotation = new Vector3(MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(0));

            actor = new Cube(engine);
            currentTexture=new Bitmap(Properties.Resources.DefaultImg);
            Bitmap normal=new Bitmap(Properties.Resources.DefaultImg);
            actor.Initialize();
            actor.LoadTexture(currentTexture);
            actor.LoadNormalMap(normal);

            lightCube = new Sphere(engine, 30, 30, true, false);
            lightCube.Initialize();
            lightCube.Scale = new Vector3(0.2f, 0.2f, 0.2f);
            sw.Start();
        }

        private void LoadShaders()
        {
            Shader def = new Shader(@"Shaders/normal.vert", @"Shaders/normal.frag", true);
            engine.Shaders.Add("Default", def);

            Shader light=new Shader(@"Shaders/light.vert", @"Shaders/light.frag", true);
            engine.Shaders.Add("Light - Phong", light);

            Shader lightGouraud = new Shader(@"Shaders/lightGouraud.vert", @"Shaders/lightGouraud.frag", true);
            engine.Shaders.Add("Light - Gouraud", lightGouraud);

            Shader lightNormalMap = new Shader(@"Shaders/light.vert", @"Shaders/lightNormalMap.frag", true);
            engine.Shaders.Add("Light Normal Mapping", lightNormalMap);

            Shader explode=new Shader(@"Shaders/explode.vert",@"Shaders/explode.frag",@"Shaders/explode.geom",true);
            engine.Shaders.Add("Explode Geometry",explode);

            Shader normals=new Shader(@"Shaders/explode.vert", @"Shaders/explode.frag", @"Shaders/normalsGeom.geom", true);
            engine.Shaders.Add("Normals Geometry",normals);

            Shader toon = new Shader(@"Shaders/toon.vert", @"Shaders/toon.frag", true);
            engine.Shaders.Add("Toon", toon);

            Shader ocean = new Shader(@"Shaders/light.vert", @"Shaders/Ocean.frag", true);
            engine.Shaders.Add("Ocean", ocean);

            engine.SelectShader("Light - Phong");


            GenerateMenuShaders();
            ShaderToTextBoxes();
        }

        private void GenerateMenuShaders()
        {
            foreach (var type in engine.Shaders)
            {
                MenuItem mItem = new MenuItem();
                mItem.Header = type.Key;
                mItem.Click += ShaderMenuItemClick;
                SampleShadersMenu.Items.Add(mItem);
            }
        }

        private void ShaderMenuItemClick(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            string shaderKey=(item.Header.ToString());
            engine.SelectShader(shaderKey);
            ShaderToTextBoxes();
        }

        private void ShaderToTextBoxes()
        {
            shaderFragTextBox.Text = engine.ActiveShader.FragmentShader;
            shaderVertexTextBox.Text = engine.ActiveShader.VertexShader;
            shaderGeomTextBox.Text = engine.ActiveShader.GeometryShader;
        }

        private void UpdateKeyboard()
        {
            if (!glControl.Focused)
                return;
            
            Vector3 moveVector = Vector3.Zero;
            KeyboardState keyState = OpenTK.Input.Keyboard.GetState();

            if (keyState.IsKeyDown(OpenTK.Input.Key.W)) 
                moveVector.Z = -1;

            if (keyState.IsKeyDown(OpenTK.Input.Key.S))
                moveVector.Z = 1;

            if (keyState.IsKeyDown(OpenTK.Input.Key.A))
                moveVector.X = -1;

            if (keyState.IsKeyDown(OpenTK.Input.Key.D))
                moveVector.X = 1;

            if (keyState.IsKeyDown(OpenTK.Input.Key.Space))
                moveVector.Y = 1;

            if (keyState.IsKeyDown(OpenTK.Input.Key.C))
                moveVector.Y = -1;

            if (keyState.IsKeyDown(OpenTK.Input.Key.Keypad8))
                Globals.lightPos.Z -= lightSpeed;

            if (keyState.IsKeyDown(OpenTK.Input.Key.Keypad5))
                Globals.lightPos.Z += lightSpeed;

            if (keyState.IsKeyDown(OpenTK.Input.Key.Keypad4))
                Globals.lightPos.X -= lightSpeed;

            if (keyState.IsKeyDown(OpenTK.Input.Key.Keypad6))
                Globals.lightPos.X += lightSpeed;

            if (keyState.IsKeyDown(OpenTK.Input.Key.Keypad7))
                Globals.lightPos.Y += lightSpeed;
                
            if (keyState.IsKeyDown(OpenTK.Input.Key.Keypad1))
                Globals.lightPos.Y -= lightSpeed;
            
            if(keyState.IsKeyDown(OpenTK.Input.Key.LShift))
                engine.Camera.AddPosition(moveVector * (float)elapsedTime * walkSpeed * 0.1f);
            else
                engine.Camera.AddPosition(moveVector * (float)elapsedTime * walkSpeed);
        }

        private void UpdateMouse()
        {
            MouseState currentMouse = OpenTK.Input.Mouse.GetState();
            if (glControl.Focused)
            {
                if (currentMouse.IsButtonDown(MouseButton.Left))
                {
                    if (currentMouse != previousMouse)
                    {
                        engine.Camera.Rotation.Y -= (currentMouse.X - previousMouse.X) * mouseAcceleration;
                        engine.Camera.Rotation.X -= (currentMouse.Y - previousMouse.Y) * mouseAcceleration;
                    }
                }
                int scrollDiff = currentMouse.ScrollWheelValue - previousMouse.ScrollWheelValue;
                if (Math.Abs(scrollDiff)>0)
                {
                    engine.Camera.AddPosition(Vector3.UnitZ * (-1) * scrollDiff * scrollAcceleration);
                }
            }
            previousMouse = currentMouse;

        }
        void ReloadShader()
        {
            if (shaderGeomTextBox.Text.Trim() != "")
            {
                Shader newShader = new Shader(shaderVertexTextBox.Text, shaderFragTextBox.Text, shaderGeomTextBox.Text);
                if (engine.Shaders.ContainsKey("custom"))
                    engine.Shaders["custom"] = newShader;
                else
                    engine.Shaders.Add("custom", newShader);
                engine.SelectShader("custom");

                LogErrors(newShader.VertexLog, newShader.FragmentLog, newShader.GeometryLog);
            }
            else
            {
                Shader newShader = new Shader(shaderVertexTextBox.Text, shaderFragTextBox.Text);
                if (engine.Shaders.ContainsKey("custom"))
                    engine.Shaders["custom"] = newShader;
                else
                    engine.Shaders.Add("custom", newShader);
                engine.SelectShader("custom");

                LogErrors(newShader.VertexLog, newShader.FragmentLog);
            }

        }
        private void LogErrors(string vertexLog, string fragmentLog,string geometryLog="")
        {
            errorFragListBox.Items.Clear();
            var fragLog = fragmentLog.Trim().Split(new[]{'\n'},StringSplitOptions.RemoveEmptyEntries);
            if (fragLog.Length > 0)
            {
                FragmentTab.Background = Brushes.PaleVioletRed;
            }
            else
            {
                FragmentTab.Background = new LinearGradientBrush(System.Windows.Media.Color.FromRgb(240, 240, 240),
                    System.Windows.Media.Color.FromRgb(229,229,229),new Point(0.5,0),new Point(0.5,1));
            }
            foreach (var row in fragLog)
            {
                errorFragListBox.Items.Add(row);
            }

            errorVertexListBox.Items.Clear();
            var vertLog = vertexLog.Trim().Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            if (vertLog.Length > 0)
            {
                VertexTab.Background = Brushes.PaleVioletRed;
            }
            else
            {
                VertexTab.Background = new LinearGradientBrush(System.Windows.Media.Color.FromRgb(240, 240, 240),
                    System.Windows.Media.Color.FromRgb(229, 229, 229), new Point(0.5, 0), new Point(0.5, 1));
            }
            foreach (var row in vertLog)
            {
                errorVertexListBox.Items.Add(row);
            }

            errorGeomListBox.Items.Clear();
            var geomLog = geometryLog.Trim().Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            if (geomLog.Length > 0)
            {
                GeometryTab.Background = Brushes.PaleVioletRed;
            }
            else
            {
                GeometryTab.Background = new LinearGradientBrush(System.Windows.Media.Color.FromRgb(240, 240, 240),
                    System.Windows.Media.Color.FromRgb(229, 229, 229), new Point(0.5, 0), new Point(0.5, 1));
            }
            foreach (var row in geomLog)
            {
                errorGeomListBox.Items.Add(row);
            }
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            ReloadShader();
        }

        private void glHost_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void shaderTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void gridSplitter_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if(ColumnDefinitionOther.Width.Value!=gridSplitter.Width)
                ColumnDefinitionOther.Width=new GridLength(gridSplitter.Width);
            else
                ColumnDefinitionOther.Width=new GridLength(0,GridUnitType.Auto);
        }

        private void gridSplitter_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {

        }

        private void GenerateObject(string name)
        {
            
            try
            {

                
                var genType = objectsDict[name];
                var actP = actor.Position;
                var actR = actor.Rotation;
                var actS = actor.Scale;
                if (CreateObjectAcceptedTypes.Contains(genType))
                {
                    CreateObject cobj = new CreateObject();
                    bool? ok = cobj.ShowDialog();
                    if (ok.HasValue && ok.Value)
                    {
                        actor = (Actor) Activator.CreateInstance(genType, engine,
                            cobj.CurvePoints, cobj.RotPoints, cobj.Smooth, cobj.Indexed);
                        actor.Initialize();
                        if (currentTexture != null)
                            actor.LoadTexture(currentTexture);
                        if (currentNormalTexture != null)
                            actor.LoadNormalMap(currentNormalTexture);
                        actor.Position = actP;
                        actor.Rotation = actR;
                        actor.Scale = actS;
                    }
                }
                else
                {
                    actor = (Actor) Activator.CreateInstance(genType, engine);
                    actor.Initialize();
                    if (currentTexture != null)
                        actor.LoadTexture(currentTexture);
                    if (currentNormalTexture != null)
                        actor.LoadNormalMap(currentNormalTexture);
                    actor.Position = actP;
                    actor.Rotation = actR;
                    actor.Scale = actS;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        

        private void LoadModelButton_OnClick(object sender, RoutedEventArgs e)
        {
            using (OpenFileDialog openDialog = new OpenFileDialog())
            {
                openDialog.DefaultExt = ".obj";
                if (openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    actor=new Model(engine,openDialog.FileName);
                    actor.Initialize();
                    if(currentTexture!=null)
                        actor.LoadTexture(currentTexture);
                }
            }
        }

        private void loadModelButton_Copy_Click(object sender, RoutedEventArgs e)
        {
            if (actor == null)
                return;

            using (SaveFileDialog fileDialog = new SaveFileDialog())
            {
                fileDialog.DefaultExt = ".ply";
                
                fileDialog.AddExtension = true;
                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    using (StreamWriter writer = new StreamWriter(fileDialog.FileName))
                    {
                        int vertexCount = 0;

                        bool indexes = actor.Indexes != null && actor.Indexes.Length > 0;

                        if (indexes)
                            vertexCount = actor.Indexes.Length;
                        else
                            vertexCount = actor.Vertices.Length; //float[] instead of vector3 fool me

                        int faceCount = vertexCount/3;
                        writer.WriteLine("ply");
                        writer.WriteLine("format ascii 1.0");
                        writer.WriteLine("element vertex {0}", vertexCount);
                        writer.WriteLine("property float x");
                        writer.WriteLine("property float y");
                        writer.WriteLine("property float z");
                        writer.WriteLine("property float nx");
                        writer.WriteLine("property float ny");
                        writer.WriteLine("property float nz");
                        writer.WriteLine("property float red");
                        writer.WriteLine("property float green");
                        writer.WriteLine("property float blue");
                        writer.WriteLine("element face {0}", faceCount); 
                        writer.WriteLine("property list uchar uint vertex_indices");
                        writer.WriteLine("end_header");
                        if (!indexes)
                        {
                            for (int i = 0; i < vertexCount; i++)
                            {
                                writer.WriteLine("{0} {1} {2} {3} {4} {5} {6} {7} {8}",
                                    actor.Vertices[i].X, actor.Vertices[i].Y, actor.Vertices[i].Z,
                                    actor.Colors[i].X, actor.Colors[i].Y, actor.Colors[i].Z,
                                    actor.Normals[i].X, actor.Normals[i].Y, actor.Normals[i].Z);

                            }
                            for (int i = 0; i < vertexCount; i=i+3)
                            {
                                writer.WriteLine("3 {0} {1} {2}", i, i + 1, i + 2);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < vertexCount; i++)
                            {
                                int ind = actor.Indexes[i];
                                writer.WriteLine("{0} {1} {2} {3} {4} {5} {6} {7} {8}",
                                    actor.Vertices[ind].X, actor.Vertices[ind].Y, actor.Vertices[ind].Z,
                                    actor.Colors[ind].X, actor.Colors[ind].Y, actor.Colors[ind].Z,
                                    actor.Normals[ind].X, actor.Normals[ind].Y, actor.Normals[ind].Z);
                            }
                            for (int i = 0; i < vertexCount; i=i+3)
                            {
                                writer.WriteLine("3 {0} {1} {2}", actor.Indexes[i], actor.Indexes[i + 1],
                                    actor.Indexes[i + 2]);
                            }
                        }
                    }
                }
            }
        }

        private void GenerateMenu_OnSubmenuClosed(object sender, RoutedEventArgs e)
        {

        }

        private void GlHost_OnDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] file = (string[])e.Data.GetData(DataFormats.FileDrop);

            }
        }

        private void LoadTexture(string path)
        {
            if (path.EndsWith(".bmp") || path.EndsWith(".png") || path.EndsWith(".jpg") ||
                path.EndsWith(".jpeg"))
            {
                BitmapImage image = new BitmapImage(new Uri(path));
                textureImage.Source = image;
                currentTexture?.Dispose();
                currentTexture=new Bitmap(path);
                actor.LoadTexture(currentTexture);
            }
        }


        private void LoadNormal(string path)
        {
            if (path.EndsWith(".bmp") || path.EndsWith(".png") || path.EndsWith(".jpg") ||
                path.EndsWith(".jpeg"))
            {
                BitmapImage image = new BitmapImage(new Uri(path));
                normalTextureImage.Source = image;
                currentNormalTexture?.Dispose();
                currentNormalTexture = new Bitmap(path);
                actor.LoadNormalMap(currentNormalTexture);
            }
        }

        private void Image_OnDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] file = (string[])e.Data.GetData(DataFormats.FileDrop);
                Image img = (Image) sender;
                if (file[0].EndsWith(".bmp") || file[0].EndsWith(".png") || file[0].EndsWith(".jpg") ||
                    file[0].EndsWith(".jpeg"))
                {
                    LoadTexture(file[0]);
                }
            }
        }

        private void Image_OnDragEnter(object sender, DragEventArgs e)
        {
            if(e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effects=System.Windows.DragDropEffects.Copy;
        }

        private void image_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            using (OpenFileDialog fileDialog = new OpenFileDialog())
            {
                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    LoadTexture(fileDialog.FileName);
                }
            }
        }

        private void NormalTextureImage_OnDragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effects = System.Windows.DragDropEffects.Copy;
        }

        private void NormalTextureImage_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            using (OpenFileDialog fileDialog = new OpenFileDialog())
            {
                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    LoadNormal(fileDialog.FileName);
                }
            }
        }

        private void NormalTextureImage_OnDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] file = (string[])e.Data.GetData(DataFormats.FileDrop);
                Image img = (Image)sender;
                if (file[0].EndsWith(".bmp") || file[0].EndsWith(".png") || file[0].EndsWith(".jpg") ||
                    file[0].EndsWith(".jpeg"))
                {
                    LoadNormal(file[0]);
                }
            }
        }


        private void ShaderVertexTextBox_OnTextChanged(object sender, EventArgs e)
        {

        }
        private void errorFragListBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var el = errorFragListBox.InputHitTest(e.GetPosition(errorFragListBox));
            var box = el as TextBlock;
            if (box != null)
            {
                string errortext = box.Text;
                int firstIndex = errortext.IndexOf('(');
                errortext = errortext.Substring(firstIndex + 1);
                int secondIndex = errortext.IndexOf(')');
                errortext = errortext.Substring(0, secondIndex);

                int line = int.Parse(errortext);
                
                shaderFragTextBox.ScrollToLine(line);
                shaderFragTextBox.CaretOffset = shaderFragTextBox.Document.Lines[line-1].Offset;
                shaderFragTextBox.Focus();

            }
        }

        private void errorVertexListBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var el = errorVertexListBox.InputHitTest(e.GetPosition(errorVertexListBox));
            var box = el as TextBlock;
            if (box != null)
            {
                string errortext = box.Text;
                int firstIndex = errortext.IndexOf('(');
                errortext = errortext.Substring(firstIndex + 1);
                int secondIndex = errortext.IndexOf(')');
                errortext = errortext.Substring(0, secondIndex);

                int line = int.Parse(errortext);

                shaderVertexTextBox.ScrollToLine(line);
                shaderVertexTextBox.CaretOffset = shaderVertexTextBox.Document.Lines[line - 1].Offset;
                shaderVertexTextBox.Focus();

            }
        }
        private void ErrorGeomListBox_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var el = errorGeomListBox.InputHitTest(e.GetPosition(errorVertexListBox));
            var box = el as TextBlock;
            if (box != null)
            {
                string errortext = box.Text;
                int firstIndex = errortext.IndexOf('(');
                errortext = errortext.Substring(firstIndex + 1);
                int secondIndex = errortext.IndexOf(')');
                errortext = errortext.Substring(0, secondIndex);

                int line = int.Parse(errortext);
                
                shaderGeomTextBox.ScrollToLine(line);
                shaderGeomTextBox.CaretOffset = shaderGeomTextBox.Document.Lines[line - 1].Offset;
                shaderGeomTextBox.Focus();

            }
        }


        private void SmoothMenu_OnClick(object sender, RoutedEventArgs e)
        {
            actor.SmoothNormals();
        }

        private void OpenUniformsHelp(object sender, RoutedEventArgs e)
        {
            StringBuilder sbuilder=new StringBuilder();
            sbuilder.AppendLine("Uniforms:");
            sbuilder.AppendLine("uniform mat4 model; - Model Matrix");
            sbuilder.AppendLine("uniform mat4 view; - View Matrix");
            sbuilder.AppendLine("uniform mat4 projection; - Projection Matrix");
            sbuilder.AppendLine("uniform vec3 lightColor; - Light Color");
            sbuilder.AppendLine("uniform vec3 lightColor; - Light Color");
            sbuilder.AppendLine("uniform vec3 lightPos; - Light Position");
            sbuilder.AppendLine("uniform vec3 viewPos; - Camera Position");
            sbuilder.AppendLine("uniform vec3 ambientalColor; - Ambiental Color");
            sbuilder.AppendLine("uniform float time; - Time in seconds");
            sbuilder.AppendLine("uniform sampler2D textureMap; - Texture");
            sbuilder.AppendLine("uniform sampler2D normMap; - Second Texture (Usually used for normal mapping)");
            MessageBox.Show(sbuilder.ToString(),"Uniforms",MessageBoxButton.OK,MessageBoxImage.Information);

        }
        private void AboutHelp(object sender, RoutedEventArgs e)
        {
            StringBuilder sbuilder = new StringBuilder();
            sbuilder.AppendLine("SlimShader (version 1.0.0)");
            sbuilder.AppendLine("Copyright © 2016 Andrei Alexandru Aldea");
            MessageBox.Show(sbuilder.ToString(), "About", MessageBoxButton.OK, MessageBoxImage.Information);

        }

        private void GenerateCustomParametric(object sender, RoutedEventArgs e)
        {
            CreateParametric createWindow=new CreateParametric();
            bool? ok=createWindow.ShowDialog();
            if (ok.HasValue && ok.Value)
            {
                Vector2 domX=new Vector2();
                Vector2 domY=new Vector2();
                NCalc.Expression sMin=new NCalc.Expression(createWindow.sMinText.Text);
                    sMin.Parameters["Pi"] = Math.PI;
                NCalc.Expression sMax=new NCalc.Expression(createWindow.sMaxText.Text);
                    sMax.Parameters["Pi"] = Math.PI;
                NCalc.Expression tMin=new NCalc.Expression(createWindow.tMinText.Text);
                    tMin.Parameters["Pi"] = Math.PI;
                NCalc.Expression tMax=new NCalc.Expression(createWindow.tMaxText.Text);
                    tMax.Parameters["Pi"] = Math.PI;
                domX.X = (float)Convert.ToDouble(tMin.Evaluate());
                domX.Y = (float)Convert.ToDouble(tMax.Evaluate());
                domY.X = (float)Convert.ToDouble(sMin.Evaluate());
                domY.Y = (float)Convert.ToDouble(sMax.Evaluate());
                var actP = actor.Position;
                var actR = actor.Rotation;
                var actS = actor.Scale;
                actor = new RevolutionSurfaceObject(engine,
                    createWindow.XExpression.Text,
                    createWindow.YExpression.Text,
                    createWindow.ZExpression.Text, domX, domY,
                    createWindow.CurvePoints, createWindow.RotPoints,
                    createWindow.Smooth, createWindow.Indexed);
                actor.Initialize();
                if (currentTexture != null)
                    actor.LoadTexture(currentTexture);
                if (currentNormalTexture != null)
                    actor.LoadNormalMap(currentNormalTexture);
                actor.Position = actP;
                actor.Rotation = actR;
                actor.Scale = actS;
            }
        }

        private void FFDMenu_Click(object sender, RoutedEventArgs e)
        {
            ffdTransform = new FFDTransformation();
            ffdTransform.Init(4, actor, engine);
        }

        private void FFDMenuStop_Click(object sender, RoutedEventArgs e)
        {
            ffdTransform = null;
        }
    }
}
